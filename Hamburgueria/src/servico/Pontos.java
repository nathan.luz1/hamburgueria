package servico;

import java.util.ArrayList;
import java.util.Scanner;

import domain.Cadastro;
import domain.Ponto;

public class Pontos {
	static Scanner leia = new Scanner(System.in);
	
	static ArrayList<Ponto> pontos = new ArrayList<Ponto>();
	
	static int idCliente = Servico.idLogado;
	
	static float desconto = 0;
	
	public static void adicionaMaster() {
		
		Ponto ponto = new Ponto(0,0);
		pontos.add(ponto);
		
	}
	
	public static void iniciaPontos(int id) {
		
		for ( Cadastro usuario : Usuarios.usuarios ) {
			if ( id == usuario.getId() ) {
				Ponto newPonto = new Ponto(id, 0);
				pontos.add(newPonto);
		
			}
		}
		
	}
	
	public static void salvaPontos(float soma) {
		
		int pontosAdd = (int) (soma / 10);
		int pontua;
		
		for ( Cadastro usuario : Usuarios.usuarios ) {
			if ( idCliente == usuario.getId() ) {
				
				for ( Ponto ponto : pontos ) {
					if ( idCliente == ponto.getIdUsuario() ) {
						int pontosAtual = ponto.getPontos();
						
						pontua = pontosAtual + pontosAdd;
						
						ponto.setPontos(pontua);
						
						System.out.println("Pontos registrados com sucesso!");
						break;
					}
					
				}
				
			}
		
		}
		
	}
	
	public static void exibePontos() {
		
		
		for ( Cadastro usuario : Usuarios.usuarios ) {
			if ( idCliente == usuario.getId() ) {
				
				for ( Ponto ponto : pontos ) {
					if ( idCliente == ponto.getIdUsuario() ) {
						
						if ( 0 == ponto.getPontos() ) {
							System.out.println( "Você ainda não possui pontos." );
							break;
							
						} else {
							System.out.println( "Você possui " + ponto.getPontos() + " pontos." );
							break;
						}
						
					}
				}
			}
		}				
		
	}
	
	public static void resgates() {
		
		System.out.println();
		System.out.println("Recompensas:");
		System.out.println();
		System.out.println("1- R$ 5 de cashback (10 pontos)");
		System.out.println("2- R$ 10 de cashback (20 pontos)" );
		System.out.println("3- R$ 20 de cashback (30 pontos)");
		
		System.out.println( "+---------------------------------+" );
		System.out.println( " 0- Voltar" );
		System.out.println( "+---------------------------------+" );
		int res = leia.nextInt();
		
		switch ( res ) {
		case 1:
			
			for (Ponto ponto : pontos ) {
				if ( idCliente == ponto.getIdUsuario() ) {
					if ( 10 <= ponto.getPontos() ) {
						
						desconto = desconto + 5;
						atualizaPontos(10);
						
						System.out.println("Resgate realizado!");
						System.out.println("Na sua próxima compra, terá um desconto de R$ 5");
						Menus.menuResgate();
						
					} else {
						System.out.println("Você ainda não possui pontos suficientes para resgatar!");
						Menus.menuResgate();
						
					}
					
					
				}
			}
			
			break;
			
		case 2:
			
			for (Ponto ponto : pontos ) {
				if ( idCliente == ponto.getIdUsuario() ) {
					if ( 20 <= ponto.getPontos() ) {
						
						desconto = desconto + 10;
						atualizaPontos(20);
						
						System.out.println("Resgate realizado!");
						System.out.println("Na sua próxima compra, terá um desconto de R$ 10");
						Menus.menuResgate();
						
					} else {
						System.out.println("Você ainda não possui pontos suficientes para resgatar!");
						Menus.menuResgate();
						
					}
					
					
				}
			}
			
			break;
			
		case 3:
			
			for (Ponto ponto : pontos ) {
				if ( idCliente == ponto.getIdUsuario() ) {
					if ( 30 <= ponto.getPontos() ) {
						
						desconto = desconto + 20;
						atualizaPontos(30);
						
						System.out.println("Resgate realizado!");
						System.out.println("Na sua próxima compra, terá um desconto de R$ 20");
						Menus.menuResgate();
						
					} else {
						System.out.println("Você ainda não possui pontos suficientes para resgatar!");
						Menus.menuResgate();
						
					}
					
					
				}
			}
			break;
		
		case 0:
			Menus.menuPonto();
			break;
			
		default:
			System.out.println("Opção inválida");
			Menus.menuResgate();
			break;
			
		}
	}
	
	public static void atualizaPontos( int pontosDesc ) {
		
		for (Ponto ponto : pontos ) {
			if ( idCliente == ponto.getIdUsuario() ) {
				int pontoAtual = ponto.getPontos();
				int pontua = pontoAtual - pontosDesc;
				
				ponto.setPontos(pontua);
				
			}
		}
		
	}
	
	public static float verificaResgate(float valor) {
		
		if ( desconto == 0 ) {
			return valor;
			
		} else {
			
			float novoTotal = valor - desconto;
			
			System.out.println( "Seu desconto do resgate foi aplicado!" );
			
			desconto = 0;
			
			return novoTotal;
		}
		
	}
	
}
