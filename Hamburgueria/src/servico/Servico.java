package servico;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Servico {
	
	static Scanner leia = new Scanner(System.in);
	
	static String usuarioLogado = null;
	static int idLogado;
	static int permissaoUsuario;
	
	static Integer[] iDs;
	static int numer = 0;
	
	static List<Float> valorTotal = new ArrayList<Float>();
	
	public static void verificaUsuario() {
		
		System.out.println("Verificando usuário...");
		
		if ( usuarioLogado == null ) {
			
			System.out.println( " Olá, você não está conectado!" );
			usuarioVazio();

		
		} else {
			System.out.println( "Usuário " + usuarioLogado + " identificado!" );
			Menus.menu();
			
		}
		
	}
	
	public static void usuarioVazio() {
		
		
		System.out.println( "+---------------------------------+" );
		System.out.println( "|            SYOBURGUER           |" );
		System.out.println( "+---------------------------------+" );
		System.out.println( " Você já possui um cadastro? " );
		System.out.println( " 1- Sim, quero acessar" );
		System.out.println( " 2- Não, quero criar uma conta" );
		int res = leia.nextInt();
		
		switch (res) {
		case 1:
			usuarioLogado = entrar();
			
			if ( usuarioLogado == null ) {
				System.out.println("Tente novamente...");
				usuarioLogado = entrar();
				
				if ( usuarioLogado == null ) {
					System.out.println("+---------------------------------+");
					System.out.println("| Excesso de tentativas!          |");
					usuarioVazio();
				
				} else {
					System.out.println("Encaminhando para o menu principal...");
					Menus.menu();
					
				}
				
				
				
			} else {
				System.out.println("Encaminhando para o menu principal...");
				Menus.menu();
				
			}
			break;
		
		case 2:
			usuarioLogado = cadastro();
			Menus.menu();
			break;
			
		default:
			System.out.println("Resposta inválida. Reconectando...");
			usuarioVazio();
			break;
				
		}
		
	}
	
	public static String entrar() {
	
		String usuario = Usuarios.entrar();

		return usuario;

	}
	
	public static String cadastro() {
		
		String usuario = Usuarios.cadastraUsuario();;
		
		System.out.println( "+---------------------------------+" );
		System.out.println( "| Cadastro realizado com sucesso! |" );
		System.out.println( "+---------------------------------+" );
		
		return usuario;
		
	}
	
	public static void sair() {
		
		usuarioLogado = null;
		idLogado = 0;
		verificaUsuario();
		
		
	}
	
	static boolean verificaPermissao() {
		boolean permite = false;
		
		permite = Usuarios.verificaPermissao(idLogado);
		
		if (permite) {
			return permite;
			
		}
		
		return permite;
		
	}
	
		
		public static void selecao( float valor ) {
			valorTotal.add(valor);
			
			finalizar();
			
		}

		public static void finalizar() {
			
			System.out.println("Quer selecionar mais uma coisa?");
			System.out.println("1- Sim");
			System.out.println("2- Não");
			int res = leia.nextInt();
			
			switch (res) {
			case 1:
				Menus.menuPedidoNovo();
				break;
				
			case 2:
				Menus.menuFinalizar();
				break;
			
			case 0:
				System.out.println(" Voltando ao menu anterior");
				Menus.menuPedidoNovo();
				
			default:
				System.out.println("Resposta inválida");
				finalizar();
				break;
				
			}
			
			leia.close();	
			
		}
		
		public static float carrinho() {
			System.out.println("Itens solicitados: ");
			
			Carrinho.listaCarrinho();
			System.out.println("");
			
			Float soma = valorTotal.stream().reduce(Float::sum).get();
			
			soma = Pontos.verificaResgate(soma);
			
	        System.out.println("O valor total dos itens é de R$ " + soma);
			  
	        
	        return soma;
	        
		}
		
		public static void preparaPedido( float soma) {
			
			boolean retorno = Pedidos.criaPedido(soma);
			
			if (retorno) {
				Pontos.salvaPontos(soma);
				Carrinho.limpaCarrinho();
				finalizaCarrinho();
				Menus.menuEntrega();
				
			} else {
				System.out.println("Não foi possível adicionar seu pedido");
				
				verificaUsuario();
				
				Menus.menu();
				
			}
			
		}
		
		public static void finalizaCarrinho() {
		
			valorTotal.clear();
			Carrinho.limpaCarrinho();
			
		}
		
		
}
