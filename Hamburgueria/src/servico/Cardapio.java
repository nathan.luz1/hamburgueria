package servico;

import java.util.ArrayList;
import java.util.Scanner;

import domain.Item;

public class Cardapio {

	static Scanner leia = new Scanner(System.in);
	
	static ArrayList<Item> xis = new ArrayList<>();
	static Item xis1 = new Item(1, "X-Salada", 18);
	static Item xis2 = new Item(2, "X-Frango", 18);
	static Item xis3 = new Item(3, "X-Strogonoff", 20);
	static Item xis4 = new Item(4, "X-Alaminuta", 23);
	static Item xis5 = new Item(5, "X-Filé", 22);
	static Item xis6 = new Item(6, "X-Tudo", 25);
	
	//Lista de porcoes 
	static ArrayList<Item> porcoes = new ArrayList<>();
	static Item porcoes1 = new Item(7, "Batata Frita", 14);
	static Item porcoes2 = new Item(8, "Batata Frita c/ Cheddar", 16);
	static Item porcoes3 = new Item(9, "Anéis de Cebola", 16);
	static Item porcoes4 = new Item(10, "Polenta Frita", 16);
	static Item porcoes5 = new Item(11, "Picadinho", 20);
 	
	//Lista de bebidas
	static ArrayList<Item> bebidas = new ArrayList<>();
	static Item bebidas1 = new Item(1, "Lata Coca", 8);
	static Item bebidas2 = new Item(2, "Lata Pepsi", 8);
	static Item bebidas3 = new Item(3, "Lata Guaraná", 8);
	static Item bebidas4 = new Item(4, "H2O", 8);
	static Item bebidas5 = new Item(5, "2 Litros Coca", 12);
	
	public static void listaXis() {
		Integer id;
		String nome;
		float valor;
		
		System.out.println( "Segue as Opções de Xis " );
		System.out.println( xis1.getId() + " " + xis1.getNome() + "  Valor R$ " + xis1.getValor() );
		System.out.println( xis2.getId() + " " + xis2.getNome() + "  Valor R$ " + xis2.getValor() );
		System.out.println( xis3.getId() + " " + xis3.getNome() + "  Valor R$ " + xis3.getValor() );
		System.out.println( xis4.getId() + " " + xis4.getNome() + "  Valor R$ " + xis4.getValor() );
		System.out.println( xis5.getId() + " " + xis5.getNome() + "  Valor R$ " + xis5.getValor() );
		System.out.println( xis6.getId() + " " + xis6.getNome() + "  Valor R$ " + xis6.getValor() );
		int res = leia.nextInt();
		
		switch (res) {
		case 1:
			id = xis1.getId();
			nome = xis1.getNome();
			valor = xis1.getValor();
			
			System.out.println("Selecionado " + id + "  " + nome + "  " + valor );
			Carrinho.adicionaCarrinho(id, nome, valor);
			Servico.selecao(valor);
			
			break;
			
		case 2:
			id = xis2.getId();
			nome = xis2.getNome();
			valor = xis2.getValor();
			
			System.out.println("Selecionado " + id + "  " + nome + "  " + valor );
			Carrinho.adicionaCarrinho(id, nome, valor);
			Servico.selecao(valor);
			
			break;
			
		case 3:
			id = xis3.getId();
			nome = xis3.getNome();
			valor = xis3.getValor();
			
			System.out.println("Selecionado " + id + "  " + nome + "  " + valor );
			Carrinho.adicionaCarrinho(id, nome, valor);
			Servico.selecao(valor);
			
			break;
			
		case 4:
			id = xis4.getId();
			nome = xis4.getNome();
			valor = xis4.getValor();
			
			System.out.println("Selecionado " + id + "  " + nome + "  " + valor );
			Carrinho.adicionaCarrinho(id, nome, valor);
			Servico.selecao(valor);
			
			break;
		
		case 5:
			id = xis5.getId();
			nome = xis5.getNome();
			valor = xis5.getValor();
			
			System.out.println("Selecionado " + id + "  " + nome + "  " + valor );
			Carrinho.adicionaCarrinho(id, nome, valor);
			Servico.selecao(valor);
			
			break;
		
		case 6:
			id = xis6.getId();
			nome = xis6.getNome();
			valor = xis6.getValor();
			
			System.out.println("Selecionado " + id + "  " + nome + "  " + valor );
			Carrinho.adicionaCarrinho(id, nome, valor);
			Servico.selecao(valor);
			
			break;
		
		case 0:
			System.out.println(" Voltando ao menu anterior");
			Menus.menuPedidoNovo();
		default: 
			System.out.println("Opção inválida. Tente novamente");
			listaXis();
			break;
			
		}
		
		leia.close();	

	}
	
	public static void listaPorcoes() {
		Integer id;
		String nome;
		float valor;
		
		System.out.println( "Segue as Porções: " );
		System.out.println( porcoes1.getId() + " " + porcoes1.getNome() + "  Valor R$ " + porcoes1.getValor() );
		System.out.println( porcoes2.getId() + " " + porcoes2.getNome() + "  Valor R$ " + porcoes2.getValor() );
		System.out.println( porcoes3.getId() + " " + porcoes3.getNome() + "  Valor R$ " + porcoes3.getValor() );
		System.out.println( porcoes4.getId() + " " + porcoes4.getNome() + "  Valor R$ " + porcoes4.getValor() );
		System.out.println( porcoes5.getId() + " " + porcoes5.getNome() + "  Valor R$ " + porcoes5.getValor() );
		int res = leia.nextInt();
		
		switch (res) {
		case 7:
			id = porcoes1.getId();
			nome = porcoes1.getNome();
			valor = porcoes1.getValor();
			
			System.out.println("Selecionado " + id + "  " + nome + "  " + valor );
			Carrinho.adicionaCarrinho(id, nome, valor);
			Servico.selecao(valor);
			
			break;
			
		case 8:
			id = porcoes2.getId();
			nome = porcoes2.getNome();
			valor = porcoes2.getValor();
			
			System.out.println("Selecionado " + id + "  " + nome + "  " + valor );
			Carrinho.adicionaCarrinho(id, nome, valor);
			Servico.selecao(valor);
			
			break;
			
		case 9:
			id = porcoes3.getId();
			nome = porcoes3.getNome();
			valor = porcoes3.getValor();
			
			System.out.println("Selecionado " + id + "  " + nome + "  " + valor );
			Carrinho.adicionaCarrinho(id, nome, valor);
			Servico.selecao(valor);
			
			break;
			
		case 10:
			id = porcoes4.getId();
			nome = porcoes4.getNome();
			valor = porcoes4.getValor();
			
			System.out.println("Selecionado " + id + "  " + nome + "  " + valor );
			Carrinho.adicionaCarrinho(id, nome, valor);
			Servico.selecao(valor);
			
			break;
		
		case 11:
			id = porcoes5.getId();
			nome = porcoes5.getNome();
			valor = porcoes5.getValor();
			
			System.out.println("Selecionado " + id + "  " + nome + "  " + valor );
			Carrinho.adicionaCarrinho(id, nome, valor);
			Servico.selecao(valor);
			
			break;
		
		case 0:
			System.out.println(" Voltando ao menu anterior");
			Menus.menuPedidoNovo();
			
		default: 
			System.out.println("Opção inválida. Tente novamente");
			listaPorcoes();
			
			break;
			
		}
		
		leia.close();	
		
	}
	
	public static void listaBebidas() {
		
		Integer id;
		String nome;
		float valor;
		
		System.out.println( "Segue as Bebidas: " );
		System.out.println( bebidas1.getId() + " " + bebidas1.getNome() + "  Valor R$ " + bebidas1.getValor() );
		System.out.println( bebidas2.getId() + " " + bebidas2.getNome() + "  Valor R$ " + bebidas2.getValor() );
		System.out.println( bebidas3.getId() + " " + bebidas3.getNome() + "  Valor R$ " + bebidas3.getValor() );
		System.out.println( bebidas4.getId() + " " + bebidas4.getNome() + "  Valor R$ " + bebidas4.getValor() );
		System.out.println( bebidas5.getId() + " " + bebidas5.getNome() + "  Valor R$ " + bebidas5.getValor() );
		System.err.println();
		System.out.println( " 0- Voltar " );
		int res = leia.nextInt();
		
		switch (res) {
		case 1:
			id = bebidas1.getId();
			nome = bebidas1.getNome();
			valor = bebidas1.getValor();
			
			System.out.println("Selecionado " + id + "  " + nome + "  " + valor );
			Carrinho.adicionaCarrinho(id, nome, valor);
			Servico.selecao(valor);
			
			break;
			
		case 2:
			id = bebidas2.getId();
			nome = bebidas2.getNome();
			valor = bebidas2.getValor();
			
			System.out.println("Selecionado " + id + "  " + nome + "  " + valor );
			Carrinho.adicionaCarrinho(id, nome, valor);
			Servico.selecao(valor);
			
			break;
			
		case 3:
			id = bebidas3.getId();
			nome = bebidas3.getNome();
			valor = bebidas3.getValor();
			
			System.out.println("Selecionado " + id + "  " + nome + "  " + valor );
			Carrinho.adicionaCarrinho(id, nome, valor);
			Servico.selecao(valor);
			
			break;
			
		case 4:
			id = bebidas4.getId();
			nome = bebidas4.getNome();
			valor = bebidas4.getValor();
			
			System.out.println("Selecionado " + id + "  " + nome + "  " + valor );
			Carrinho.adicionaCarrinho(id, nome, valor);
			Servico.selecao(valor);
			
			break;
		
		case 5:
			id = bebidas5.getId();
			nome = bebidas5.getNome();
			valor = bebidas5.getValor();
			
			System.out.println("Selecionado " + id + "  " + nome + "  " + valor );
			Carrinho.adicionaCarrinho(id, nome, valor);
			Servico.selecao(valor);
			
			break;
		
		case 0:
			System.out.println(" Voltando ao menu anterior");
			Menus.menuPedidoNovo();
			
		default: 
			System.out.println(" Opção inválida. Tente novamente ");
			listaPorcoes();
			
			break;
			
		}
		
		leia.close();
		
	}
	
}
