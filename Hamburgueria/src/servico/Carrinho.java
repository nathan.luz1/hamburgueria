package servico;

import java.util.ArrayList;

import domain.Item;

public class Carrinho {

	static ArrayList<Item> carrinho = new ArrayList<Item>();
	static ArrayList<Item> carrinhoSalvo = new ArrayList <Item>();

	public static void adicionaMaster() {
		
		Item newItem = new Item(0, "X-salada", 18);
		Item newItem2 = new Item(0, "X-Alaminuta", 23);
		
		carrinhoSalvo.add(newItem);
		carrinhoSalvo.add(newItem2);
		
	}
	
	public static void adicionaCarrinho(int id, String lanche, float valor) {
		
		Item newItem = new Item(id, lanche, valor);
		carrinho.add(newItem);
		
	}
	
	public static void listaCarrinho() {
		
		System.out.println("");
		
		for ( Item i : carrinho )
		System.out.println(i.getNome() + "   R$ " + i.getValor());
		
	}
	
	public static boolean salvar(int idPedido) {
		
		for ( Item i : carrinho ) {
			Item newItem = new Item(idPedido, i.getNome(), i.getValor());
			carrinhoSalvo.add(newItem);
			
			return true;
			
		}
		
		System.out.println("Ocorreu um erro ao tentar salvar o carrinho!");
		return false;
	
	}
	
	public static void limpaCarrinho() {
		carrinho.clear();
		
	}

	static void listaCarrinhoId ( int id ) {
		
		boolean retorno = validaId(id);
		
		if (retorno) {
			for ( Item item : carrinhoSalvo ) {
				if (id == item.getId()) {
					System.out.println(item.getNome() + " R$ " + item.getValor());
				}
			}
			
		} else {
			
			System.out.println( "Nenhum lanche encontrado para o pedido selecionado" );
		}
		
		
	}
	
	static boolean validaId(int id) {
		
		for ( Item item : carrinhoSalvo ) {
			if (id == item.getId()) {
				return true;
			}
		}
		
		return false;
		
	}
	
}
