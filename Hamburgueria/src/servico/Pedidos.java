package servico;

import java.util.ArrayList;

import domain.Pedido;

public class Pedidos {
	
	static int ultPedido;
	
	static ArrayList<Pedido> pedidos = new ArrayList<Pedido>();
	
	public static void adicionaMaster() {
		String nome = Servico.usuarioLogado;
		int id = Servico.idLogado;
		float soma = 55;
		String status = "GERADO PELO MASTER";
		
		Pedido newPedido = new Pedido(1, nome, id, soma, status);
		pedidos.add(newPedido);
		
	}
	
	static boolean criaPedido(float soma) {
		String usuarioLogado = Servico.usuarioLogado;
		int idLogado = Servico.idLogado;
		String status = "PEDIDO GERADO";
		
		if (pedidos.size() >= 5 ) {
			System.out.println("O limite máximo de pedidos foi atingido!");
			System.out.println("Não estamos aceitando mais pedidos. Desculpe a inconveniência!");
			return false;
		
		} else {
			Pedido newPedido = new Pedido(2, usuarioLogado, idLogado, soma, status);
			pedidos.add(newPedido);
			
			for (Pedido i : pedidos) {
				if (usuarioLogado.equals(i.getNomeCliente()) && idLogado == i.getIdCliente() && soma == i.getTotal() && status.equals(i.getStatus()) ) {
					int idPedido = i.getId();
					Carrinho.salvar(idPedido);
					ultPedido = idPedido;
					
					return true;
				}
			
			}
		
		}
		
		System.out.println("Ocorreu um erro ao tentar registrar o pedido!");
		System.out.println("Tentando novamente...");
		return criaPedido(soma);
		
	}
	
	static boolean verificaPedidos() {
		
		for ( Pedido pedido : pedidos ) {
			if ( Servico.idLogado == pedido.getIdCliente()) {
				return true;
				
			}
		}
		
		System.out.println("Nenhum Pedido registrado");
		return false;
	}
	
	static void listaPedidos() {
		
		System.out.println( "+---------------------------------+" );
		System.out.println( "|            SYOBURGUER           |" );
		System.out.println( "|             Pedidos             |" );
		System.out.println( "+---------------------------------+" );
		
		boolean verifica = verificaPedidos();
		
		if (verifica) {
			for ( Pedido i : pedidos ) {
				if ( Servico.idLogado == i.getIdCliente() ) {
				
					System.out.println("Pedido: " + i.getId() + " | Cliente: " + i.getNomeCliente() + " | R$ " + i.getTotal() + " | STATUS: " + i.getStatus() );
				
				}
				
			}
		
		
		} else {
			System.out.println("");
		}

	}
	
	static boolean verificaPedidoUsuario(int id) {
		
		for ( Pedido pedido : pedidos)  {
			if ( id == pedido.getId() ) {
				if ( Servico.idLogado == pedido.getIdCliente() ) {
					
					return true;
					
				}
				
			}
		}
		
		return false;
	}
	
	static void listaPedidosLanches(int id) {
		
		for ( Pedido pedido : pedidos)  {
			if ( id == pedido.getId() ) {
				System.out.println("Pedido: " + pedido.getId() + " Cliente: " + pedido.getNomeCliente() + " R$ " + pedido.getTotal() + " STATUS: " + pedido.getStatus() );
				Carrinho.listaCarrinhoId(id);
				
			}
		}
		
		
	}
	
	
	static boolean cancelaPedido ( int id ) {
		
		boolean verifica = verificaPedidos();
		
		if ( verifica ) {
			for ( Pedido pedido : pedidos)  { 
				if ( id == pedido.getId() ) {
					String statusAtual = pedido.getStatus();
					
					if ( statusAtual.equals( "PEDIDO CANCELADO" ) ) {
						System.out.println( "O pedido já foi cancelado!" );
						return true;
						
					} else {
						if ( statusAtual.equals( "SAIU PARA ENTREGA" ) ) {
							System.out.println( "O pedido não pode ser cancelado" );
							System.out.println( "Pois já saiu para entrega!" );
							
						} else {
							if ( statusAtual.equals( "ENTREGUE" ) ) {
								System.out.println( "O pedido não pode ser cancelado" );
								System.out.println( "Pois já foi entregue!" );
							}
							
						}
						
					}
					
					
					pedido.setStatus("PEDIDO CANCELADO");
					System.out.println("Pedido cancelado com sucesso!");
					return true;
					
					
				}
			
			}
		}
		
		System.out.println("Não foi possível localizar o pedido de ID: " + id);
		return false;
	}
	
	static boolean alteraStatus( int id, String status) {
		
		if (status.equals("")) {
			
		} else {
			for ( Pedido pedido : pedidos)  { 
				if ( id == pedido.getId() ) {
					
					if ( pedido.getStatus().equals(status.toUpperCase())) {
						System.out.println("Status é identico ao atual!");
						
						return false;
						
					} else {
						pedido.setStatus(status.toUpperCase());
						System.out.println("Status alterado com sucesso!");
						
						return true;
						
						
					}
							
				}
			}
		}
		
		
		
		System.out.println("Não foi possível localizar o pedido de ID: " + id);
		return false;
		
	}
	
	
	static float ultimoTotal() {
		float total = 0;
		
		for ( Pedido i : pedidos ) {
			if ( ultPedido == i.getId() ) {
				total = i.getTotal();
				return total;
			}
			
			
		}
		
		return total;
	}
			
	
}
