package domain;

public class Ponto {
	
	int idUsuario;
	int pontos;
	
	public Ponto(int idUsuario, int pontos) {
		this.idUsuario = idUsuario;
		this.pontos = pontos;
	}
	
	
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public int getPontos() {
		return pontos;
	}
	public void setPontos(int pontos) {
		this.pontos = pontos;
	}
	
	
}
