package domain;

public class Contato {

	int id;
	String Rua;
	String numero;
	String Bairro;
	String telefone;
	int idCadastro;

	public Contato(int id, String rua, String numero, String bairro, String telefone, int idCadastro) {
		super();
		this.id = id;
		Rua = rua;
		this.numero = numero;
		Bairro = bairro;
		this.telefone = telefone;
		this.idCadastro = idCadastro;
	}

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRua() {
		return Rua;
	}

	public void setRua(String rua) {
		Rua = rua;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getBairro() {
		return Bairro;
	}

	public void setBairro(String bairro) {
		Bairro = bairro;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public int getIdCadastro() {
		return idCadastro;
	}

	public void setIdCadastro(int idCadastro) {
		this.idCadastro = idCadastro;
	}
	
	
}
