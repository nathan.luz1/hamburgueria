package main;

import servico.Carrinho;
import servico.Pedidos;
import servico.Pontos;
import servico.Servico;
import servico.Usuarios;

public class Main {
	
	public static void main(String[] args) {
		
		System.out.println( "+---------------------------------+" );
		System.out.println( "|            SYOBURGUER           |" );
		System.out.println( "|    Seu acelerador de pedidos    |" );
		System.out.println( "+---------------------------------+" );
		
		Usuarios.adicionaMaster();
		Pedidos.adicionaMaster();
		Carrinho.adicionaMaster();
		Pontos.adicionaMaster();
		
		Servico.verificaUsuario();
		
	}

	
}
